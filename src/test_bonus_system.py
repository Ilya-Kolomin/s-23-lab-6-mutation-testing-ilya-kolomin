from bonus_system import calculateBonuses
import random

amount_ranges = [
    ((0, 10000), 1),
    ((10000, 50000), 1.5),
    ((50000, 100000), 2),
    ((100000, 1000000), 2.5)
]

amount_corners = [
    (9999, 1),
    (10000, 1.5),
    (10001, 1.5),

    (49999, 1.5),
    (50000, 2),
    (50001, 2),

    (99999, 2),
    (100000, 2.5),
    (100001, 2.5),
]

programs = [
    ("Standard", 0.5),
    ("Premium", 0.1),
    ("Diamond", 0.2),
    ("Nonsense", 0),
    
    ("Rtandard", 0),
    ("Ttandard", 0),
    ("Oremium", 0),
    ("Qremium", 0),
    ("Ciamond", 0),
    ("Eiamond", 0),
]

def test_ranges():
    for program, bonus in programs:
        for amount_range, multiplier in amount_ranges:
            amount = random.randrange(*amount_range)
            assert calculateBonuses(program, amount) == (bonus * multiplier)

def test_corners():
    for program, bonus in programs:
        for amount, multiplier in amount_corners:
            assert calculateBonuses(program, amount) == (bonus * multiplier)
